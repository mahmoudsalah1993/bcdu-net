from PIL import Image
import os
import numpy as np

def create_mask_image(file_name, output_name, src, destination):
    img = Image.open(src + file_name)
    thresh = 0
    fn = lambda x : 255 if x > thresh else 0
    r = img.convert('L').point(fn, mode='1')
    rarray = np.asarray(r).astype(int)
    for x in range(rarray.shape[0]):
        for y in range(rarray.shape[1]):
            rarray[x][y] = 255 if rarray[x][y] == 1 else 0
    rarray = rarray.astype(np.uint8)
    Image.fromarray(rarray).save(destination + output_name + '_mask.png')

original_imgs_train = "./DRIVE/training/images/"
original_imgs_test = "./DRIVE/test/images/"
borderMasks_imgs_train = "./DRIVE/training/mask/"
borderMasks_imgs_test = "./DRIVE/test/mask/"

for path, subdirs, files in os.walk(original_imgs_train): #list all files, directories in the path
        for i in range(len(files)):
            #original
            create_mask_image(files[i], files[i].split('.')[0], original_imgs_train, borderMasks_imgs_train)

for path, subdirs, files in os.walk(original_imgs_test): #list all files, directories in the path
        for i in range(len(files)):
            #original
            create_mask_image(files[i], files[i].split('.')[0], original_imgs_test, borderMasks_imgs_test)
